
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ACER
 */
public class Algorithm10 {

    public static void main(String[] args) {
        int[] A = {1, 2, 4, 5, 6, 8, 4, 7, 9, 3, 4, 5, 6, 7, 8, 9, 4, 5, 8, 7, 2, 6, 4, 8, 3, 5, 9, 1, 4, 7, 8, 5, 6,
            2, 5, 4, 1, 2, 5, 4, 8, 7, 6, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
            26};

        ArrayList<Integer> longestSA = new ArrayList<>();
        ArrayList<Integer> tmp = new ArrayList<>();

        longestSA.add(A[0]);
        int n = 0;
        boolean flag = true;

        while (n < A.length - 1) {
            if (flag) {
                tmp.clear();
                tmp.add(A[n]);
                flag = false;
            }
            if (A[n] == A[n + 1] - 1) {
                tmp.add(A[n + 1]);
                if (tmp.size() > longestSA.size()) {
                    longestSA = (ArrayList<Integer>) tmp.clone();
                }
            } else {
                flag = true;
            }
            n++;
        }
        System.out.println(longestSA);
    }
}
